;	DEBUG commannd	V0.60	Aug. 2020
;	This code has been adapted to work with sjasmplus assembler
;	Author: Copyright (C) Ariakirasoft Software, assembler
;		logic and bugfixes written by Emily82 (Dec. 2019)
;		Many thanks to GuyveR800 for his patience and big help.

	output	debug.com

samejr	macro	x,y
	cp	x
	jr	z,y
	endm
samejp	macro	x,y
	cp	x
	jp	z,y
	endm
sameret	macro	x
	cp	x
	ret	z
	endm
jrdez	macro	x
	ld	a,d
	or	e
	jr	z,x
	endm
jrhlz	macro	x
	ld	a,h
	or	l
	jr	z,x
	endm
jpdez	macro	x
	ld	a,d
	or	e
	jp	z,x
	endm
jphlz	macro	x
	ld	a,h
	or	l
	jp	z,x
	endm

print	macro	x
	ld	a,x
	call	putch
	endm
println	macro
	call 	putret
	endm
getaf	macro
	ld	hl,(reg_area)
	push	hl
	pop	af
	endm
regmac	macro	x,y
	cp	x
	jr	z,y
	inc	hl
	endm
; MACROS for asm parser

; Writes byte v to mem location pointed by (adres)
; and increment (adres)
wrtb 	macro	v
	push	hl
	push	af
	ld	a, v
	call	inc_adres
	pop	af
	pop	hl
	endm
setx	macro	v
	; reset old x
	and	3fh
	; set new x (v << 6)
	or	v * 64
	endm
sety	macro	v
	; reset old y
	and	0c7h
	; set new x (v << 8)
	or	v * 8
	endm
setz	macro	v
	; reset old z
	and	0f8h
	; set new z
	or	v
	endm
setp	macro	v
	; reset old p
	and	0CFh
	; set new p (v << 4)
	or	v * 16
	endm
setq	macro	v
	; reset old q
	and	0f7h
	; set new q (v << 3)
	or	v * 8
	endm
; Sets y value in b to a register
yfrom	macro	reg
	; reset old y
	and	0c7h
	sla	reg
	sla	reg
	sla	reg
	; set new y
	or	reg
	endm
pfrom	macro	reg
	; reset old y
	and	0CFh
	sla	reg
	sla	reg
	sla	reg
	sla	reg
	; set new y
	or	reg
	endm

; ROUTINES
_	equ	0
calslt	equ	0001ch
fcb	equ	0005ch
chget	equ	0009fh
chput	equ	000a2h
inifnk	equ	0003eh
fnksb	equ	000c9h
erafnk	equ	000cch
dspfnk	equ	000cfh
chgcpu	equ	00180h
getcpu	equ	00183h
; SYSVARS
fnkstr	equ	0f87fh
exptbl	equ	0fcc1h

; CONSTANTS
bufsz	equ	60
strsz	equ	bufsz + 2

; SPECIAL ADDRESSES
SYSTEM	equ	5
_BUFIN	equ	0ah
rdslt	equ	0ch

;	.z80
;	aseg
	org	100h

main_adres 	equ	0a000h

install:
	ld	hl,start
	ld	de,main_adres
	ld	bc,last-start
	ldir
	ld	(reg_sp@),sp
	pop	hl
	ld	(stack_top),sp
	push	hl

	ld	a,(fcb+1)
	cp	32
	jp	z,comlin
	ld	de,0100h
	jp	file_load

start:	phase	main_adres

comlin:	jr	command_line

adres_prog:	defw	0100h
stack_top:	defw	0000h
reset_adres:	defw	0000h
break_point:	defs	10*3+2

command_line:
	print	'-'
	ld	de,128
	ld	a,126
	ld	(de),a
	ld	c,0ah
	call	5
	print	10

	ld	hl,129
	ld	a,(hl)
	inc	l
	add	a,l
	ld	l,a
	ld	(hl),0
	ld	de,130

com_jump:
	ld	a,(de)
	inc	de
	or	a
	jr	z,comlin
	cp	' '
	jr	z,com_jump
	cp	'?'
	jp	z,help
	cp	'#'
	jp	z, cpumode
	or	32
	cp	'w'+1
	jp	nc,error1
	sub	'a'
	jp	c,error1
	add	a,a
	ld	hl,com_table
	add	a,l
	ld	l,a
	ld	a,h
	adc	a,0
	ld	h,a
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	ld	a,b
	or	c
	jp	z,error1
	push	bc
	ret


; ****  INCLUDE FILES  ****

	include "subs.asm"	; subroutines
	include "cmds.asm"	; commands


; **** error *****

error1:	inc	de
error0:	ld	hl,-131
	add	hl,de
	ld	b,l
	ld	c,2
error2:	push	bc
	ld	e,32
	call	5
	pop	bc
	djnz	error2
	ld	de,errmes
error3:	ld	c,9
	call	5
	ld	sp,(reg_sp@)
	jp	comlin
error:	ld	de,errmes+2
	jr	error3


; ****  Sub routines  ****

get_range:
	call	getwrd
	inc	a
	jr	z,error
	push	hl
	call	getwrd
	pop	bc
	inc	a
	jr	z,error
	or	a
	sbc	hl,bc
	jr	c,error
	inc	hl
	ret

putwrd:	ld	a,h
	call	puthex
	ld	a,l
	call	puthex
	;print	'h'
	ret

; prints ASCII$
putlin:	ld	a,(de)
	inc	de
	cp	'$'
	ret	z
	call	putch
	jr	putlin

; For msx turbo r only
; In: A = cpu mode
; 0 : Z80
; 1 : R800 ROM
; 2 : R800 DRAM
setcpu:
	or	80h		; turbo led
	ld	ix, chgcpu
	ld	iy, (exptbl-1)
	jp	calslt

; prints n characters (n = reg B)
putstr:	print	(de)
	inc	de
	djnz	putstr
	ret

puttab:	ld	a,9
	jr	putch
putcon:	ld	a,','
	jr	putch
hexspc:	call	puthex		; put hex & space
putspc:	ld	a,32		; put space
putch:	ld	ix,chput
putch2:	ld	iy,(exptbl-1)
	jp	calslt

getch:	ld	ix,chget
	jr	putch2

; In:	A is fill char value
;	B is size of buffer
;	DE buffer location
; Out:	resetted buffer
; Use:	B, DE, F
reset_buf:
	ld	(de), a
	inc	de
	djnz	reset_buf
	ret


hexout:	push	af		; put hex
	rra
	rra
	rra
	rra
	call	hexou1
	pop	af
hexou1:	and	15
hexput:	add	a,'0'
	cp	'9'+1
	jr	c,putch
	add	a,'@'-'9'
	jr	putch

puthex:	push	hl
	push	de
	push	bc
	call	hexout
	pop	bc
	pop	de
	pop	hl
	ret

put10:	push	hl
	push	de
	push	bc
	exx
	ld	hl,put10dat
	exx
	ld	de,chrwrk
put10a:	exx
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	inc	hl
	push	bc
	exx
	pop	bc
	xor	a
put10b:	inc	a
	add	hl,bc
	jr	c,put10b
	sbc	hl,bc
	add	a,'0'-1
	ld	(de),a
	inc	de
	ld	a,b
	and	c
	inc	a
	jr	nz,put10a
	ld	a,'$'
	ld	(de),a
	ld	de,chrwrk
put10c:	ld	a,(de)
	inc	de
	cp	'0'
	jr	z,put10c
	cp	'$'
	jr	nz,put10d
	dec	de
put10d:	dec	de
	call	putlin
	pop	bc
	pop	de
	pop	hl
	ret

putbin:	push	bc
	ld	b,8
putbin@:rlca
	ld	c,a
	ld	a,'0'
	adc	a,0
	call	putch
	ld	a,c
	djnz	putbin@
	pop	bc
	ret

getwrd:	ld	hl,0
getwr1:	ld	a,(de)
	inc	de
	cp	32
	jr	z,getwr1
	or	a
	ld	a,255
	ret	z	; no parameter = 255
	dec	de
getwr2:	ld	a,(de)	; not space
	or	a
	ret	z	; end mark = 0
	inc	de
	cp	32
	ret	z	; space = 32
;	cp	9
;	ret	z	; tab = 9
	call	hexcheck
	jr	c,get_e
	add	hl,hl
	jr	c,get_e
	add	hl,hl
	jr	c,get_e
	add	hl,hl
	jr	c,get_e
	add	hl,hl
	jr	c,get_e
	add	a,l
	ld	l,a
	jr	getwr2
get_e:	pop	hl
	jp	error1

; In: 	A is digit to check
; Out:	CY=0 and A=0-15 if valid, CY=1 otherwise
; Use:	A, F
; Cmt:	Checks if character in A reg. is valid hex digit
hexcheck:
	cp	'0'
	ret	c
	cp	'9'+1
	jr	c,hexch3
	or	32
	cp	'a'
	ret	c
	cp	'f'+1
	jr	c,hexch2
	scf
	ret
hexch2:	sub	'a'-'9'-1
hexch3:	sub	'0'
	ret

; In:	HL points to digits string
; Out:	CY=0 and A=<number> if valid, CY=1 otherwise
; 	HL is altered if CY=0
; Use:	A, HL, F
; Cmt:	Checks if the string is valid hex byte number
hexcheck_byt:
	push	hl
	push	bc
	push	de
	ex	de, hl
	ld	hl, 0
	ld	b, 0
hexcheck_by1:
	ld	a, (de)
	cp	'$'
	jr	z, hexcheck_bchk
	cp	13
	jr	z, hexcheck_bchk
	cp	','
	jr	z, hexcheck_bchk
	cp	')'
	jr	z, hexcheck_bchk
	inc	de
	call	hexcheck
	jr	c, hexcheck_bfail
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	a, l
	ld	l, a
	inc	b
	jr	hexcheck_by1
hexcheck_bchk:			; at least 1 digit or fail
	ld	a, b
	cp	1
	jr	c, hexcheck_bfail
	cp	3		; max 2 digits
	jr	nc, hexcheck_bfail
	ld	a, l
	jr	hexcheck_bok
hexcheck_bok:
	ex	de, hl
	pop	de
	pop	bc
	inc	sp		; dont restore HL
	inc	sp
	or	a		; clear carry
	ret
hexcheck_bfail:
	ex	de, hl
	pop	de
	pop	bc
	pop	hl
	scf			; to be sure :)
	ret

; In:	HL points to digits string
; Out:	CY=0 and DE=<number> if valid, CY=1 otherwise
;	HL is altered if CY=0
; Use:	A, HL, F
; Cmt:	Checks if the string is valid hex word number
hexcheck_wrd:
	push	hl
	push	bc
	ex	de, hl
	ld	hl, 0
	ld	b, 0
hexcheck_wr1:
	ld	a, (de)
	cp	'$'
	jr	z, hexcheck_wchk
	cp	13
	jr	z, hexcheck_wchk
	cp	','
	jr	z, hexcheck_wchk
	cp	')'
	jr	z, hexcheck_wchk
	inc	de
	call	hexcheck
	jr	c, hexcheck_wfail
	add	hl, hl
	jr	c, hexcheck_wfail
	add	hl, hl
	jr	c, hexcheck_wfail
	add	hl, hl
	jr	c, hexcheck_wfail
	add	hl, hl
	jr	c, hexcheck_wfail
	add	a, l
	ld	l, a
	inc	b
	jr	hexcheck_wr1
hexcheck_wchk:			; at least 1 digit or fail
	ld	a, b
	cp	1
	jr	nc, hexcheck_wok
	jr	hexcheck_wfail
hexcheck_wok:
	ex	de, hl
	pop	bc
	inc	sp		; dont restore HL
	inc	sp
	or	a		; clear carry
	ret
hexcheck_wfail:
	ex	de, hl
	pop	bc
	pop	hl
	scf			; to be sure :)
	ret

putret:	print	13
	print	10
	ret

upper:	cp	'a'
	ret	c
	cp	'z'+1
	ret	nc
	sub	32
	ret

; In:	DE points to string to upper
; Out:	String with upper chars
; Use:	A, B, C, DE, F
; Cmt:  this function does not capitalize
;	characters between quote characters
str_upper:
	ld	b, 0
str_upper_loop:
	ld	a, (de)
	sameret	13
	samejr	'$', str_upper_chk
	samejr	39, str_upper_tgl
str_upper_cont:
	ld	c, a
	ld	a, b
	cp	0
	jr	nz, str_upper_skp
	ld	a, c
	call	upper
	ld	(de), a
str_upper_skp:
	inc	de
	jr	str_upper_loop
str_upper_tgl:
	ex	af, af'
	ld	a, 1
	xor	b
	ld	b, a
	ex	af, af'
	jr	str_upper_cont
str_upper_chk:
	ld	c, a
	ld	a, b
	sameret 0			; $ inside quotes are NOT end of string
	ld	a, c
	jr	str_upper_cont

; In: 	HL src string
;	DE dest string
; Out:	B contains num of copied chars
; Use:	A, B, C, HL, DE, F
strcpy:
	ld	bc, 0
strcpy_loop:
	ld	a, (hl)
	samejr	'$', strcpy_chk
	samejr	13, strcpy_ret
	cp	39
	jr	nz, strcpy_cont
	ex	af, af'
	ld	a, 1
	xor	c
	ld	c, a
	ex	af, af'
strcpy_cont:
	ld	(de), a
strcpy_cont2:
	inc	hl
	inc	de
	inc	b
	jr 	strcpy_loop
strcpy_chk:
	ld	(de), a
	ld	a, c
	samejr	1, strcpy_cont2		; $ inside quotes are NOT end of string
	ret
strcpy_ret:
	ld	(de), a
	ret

; In:	HL points to string
; Out:	string is spaces stripped
; Use:	A, HL, DE, B, F
; Cmt: 	remove all spaces from string
; 	except the first one and spaces
;	inside quote strings
; Note: after this input string is an ASCII$
;	without carriage return!
strip_spc:
	; reset tmp_buf
	ld	de, tmp_buf
	ld	b, strsz
	ld	a, '$'
	call	reset_buf
	push	hl
	xor	a
	; b : atleast a space has been read?
	; c : strip space toggle
	ld	bc, 0
	ld	de, tmp_buf
strip_spc_loop:
	ld	a, (hl)
	samejr	39, strip_spc_tgl	; 39 -> (') character
	samejr	13, strip_spc_ret
	samejr	'$', strip_spc_chk
	samejr	' ', strip_spc_flag
	jr	strip_spc_loop2
strip_spc_tgl:
	ex	af, af'
	ld	a, 1
	xor	c
	ld	c, a
	ex	af, af'
strip_spc_loop2:
	ld	(de), a
	inc	de
	inc	hl
	jr	strip_spc_loop
strip_spc_flag:
	ex	af, af'
	xor	a
	cp	b
	jr	nz, strip_spc_skp
	ld	b, 1
	ex	af, af'
	jr	strip_spc_loop2
strip_spc_skp:
	xor	a
	cp	c
	ld	a, ' '
	jr	nz, strip_spc_loop2
	inc	hl
	jr	strip_spc_loop
strip_spc_chk:
	ld	a, c
	samejr	0, strip_spc_ret
	ld	a, '$'
	jr	strip_spc_loop2		; $ inside quotes are NOT end of string
strip_spc_ret:
	ld	hl, tmp_buf		; copy from stripped string
	pop	de			; to original string
	call	strcpy
	ld	a, b			; good idea to update (nchars)
	ld	(nchars), a
	ld	a, '$'
	ld	(de), a
	ret

; function called by wrtb macro
; to operate 16bit increment
; for (adres) variable
; A = value to store in (adres) location
inc_adres:
	ld	hl, (adres)
	ld	(hl), a
	ld	hl, adres
	inc	(hl)			; increment (adres) LSB
	xor	a
	cp	(hl)
	jr	z, inc_adres_msb	; LSB overflow!
	ret
inc_adres_msb:
	inc	hl
	inc	(hl)
	ret

; ****  Work area  ****

com_table:	defw	asm,	_,	comp,	dump,	enter,	fill	; abcdef
		defw	go,	hex,	input,	_,	key,	load	; ghijkl
		defw	move,	name,	output,	_,	quit,	register; mnopqr
		defw	search,	trace,	unasm,	_,	write		; stuvw

unadrs:	defw	unnop,unld2,unld3,uninc2,uninc1,undec1,unld4,unrlca
	defw	unnop,unadd2,unld5,undec2,uninc1,undec1,unld4,unrlca
	defw	unretc,unpop,unjpcc,unjp,uncalc,unpush,unadd3,unrst
	defw	unretc,unret,unjpcc,unjp,uncalc,uncall,unadd3,unrst

adres:		defw	0100h
last_port:	defb	0
@ixiy:		defb	0
print_line:	defb	8
put10dat:	defw	-10000,-1000,-100,-10,-1
errmes:		defm	'^ Error'
return:		defb	13,10,'$'
trmes:		defm	'This command is for MSX turboR only.',13,10,'$'
tpames:		defm	'Free TPA: 0100-$'

reg_area:	defw	0,0,0,100h,0	; AF BC DE HL IX
		defw	0,0,0,0,0	; AF'BC'DE'HL'IY
reg_sp@:	defw	0c200h		; SP
reg_mes:	defm	'AF $BC $DE $HL $IX $'
		defb	13,10,'A','F',27h,'$BC',27h,'$DE',27h,'$HL',27h,'$'
		defb	'IY $',13,10
		defm	'SP $   Flag ($'
buffer:		defb	bufsz		; size of buffer
nchars:		defb	0		; num. of entered chars
str_buf:	defs	strsz, '$'
tmp_buf:	defs	strsz, '$'
tmp_ptr:	defw	0
tmp_var:	defb	0
tmp_var2:	defb	0


err:	defb	'???',13,10,'$'
_out2:	defm	'),A$'
_sbc@:	defm	'SBC	HL,$'
_adc@:	defm	'ADC	HL,$'
_out@:	defm	'OUT	(C),$'
_exaf:	defm	"EX	AF,AF'$"
_exdh:	defm	'EX	DE,HL$'
_exsh:	defm	'EX	(SP),$'
_ldsh:	defm	'LD	SP,$'
_cc:	defm	'NZ$Z$ NC$C$ PO$PE$P$ M$'
_rr:	defm	'BC$DE$HL$IX$IY$SP$AF$'
_reg:	defm	'BCDEHLF'
_acon:	defm	'A,$'
_in@:	defm	',(C)$'
_mulub:	defb	'MULUB',9,'A,$'
_muluw:	defb	'MULUW',9,'HL,$'
_ret:	defm	'RET$'
_neg:	defm	'NEG$'
_exx:	defm	'EXX$'
_ld:	defm	'LD$CP$'
_in:	defm	'IN$OT$'
_out:	defm	'OUT$'
_im:	defb	'IM$'
_push:	defm	'PUSH$'
_pop:	defm	'POP$'
_inc:	defm	'INC$'
_dec:	defm	'DEC$'
_math:	defm	'ADD$ADC$SUB$SBC$AND$XOR$OR$ CP$ '
_rst:	defm	'RST$'
; BUGFIX: RES and SET were inverted
_bit:	defm	'BIT$RES$SET$'
_rlc:	defm	'RLC$RRC$RL$ RR$ SLA$SRA$SLL$SRL$'
_rlca:	defm	'RLCA$RRCA$RLA$	RRA$ DAA$ CPL$ SCF$ CCF$'
_djnz:	defm	'DJNZ$'
_jp:	defm	'JP$'
_jr:	defm	'JR$'
_call:	defm	'CALL$'
_halt:	defm	'HALT$'
_di:	defm	'DI$'
_ei:	defm	'EI$'
_nop:	defm	'NOP$'

; R800 IXH, IXL
_rx:		defb	'B$C$D$E$'
_ixh:		defb	'IXH$'
_ixl:		defb	'IXL$'
_rx2:		defb	' $A$', 0
; R800 IYH, IYL
_ry:		defb	'B$C$D$E$'
_iyh:		defb	'IYH$'
_iyl:		defb	'IYL$'
_ry2:		defb	' $A$', 0

; Asm registers/flags table
_r:		defb	'B$C$D$E$H$L$(HL)$A$', 0
_rp:		defb	'BC$DE$HL$SP$', 0
_rp2:		defb	'BC$DE$HL$AF$', 0
_rp3:		defb	'(BC)$(DE)$', 0
_cc2:		defb	'NZ$Z$NC$C$PO$PE$P$M$', 0
_ldr:		defb	'HL$A$', 0

; Instructions with known opcode (no second stage processing)
imm_unprefx:	defb	'NOP$', 	000h	; <mnemonic>, <opcode>
		defb	'LD (BC),A$',	002h
		defb	'RLCA$', 	007h
		defb	"EX AF,AF'$", 	008h
		defb	'LD A,(BC)$', 	00Ah
		defb	'RRCA$', 	00Fh
		defb	'LD (DE),A$',	012h
		defb	'RLA$', 	017h
		defb	'LD A,(DE)$',	01Ah
		defb	'RRA$', 	01Fh
		defb	'DAA$',		027h
		defb	'CPL$',		02Fh
		defb	'SCF$',		037h
		defb	'CCF$',		03Fh
		defb	'HALT$', 	076h
		defb	'RET$',		0C9h
		defb	'EXX$',		0D9h
		defb	'EX (SP),HL$',	0E3h
		defb	'JP (HL)$',	0E9h
		defb	'JP HL$',	0E9h
		defb	'EX DE,HL$',	0EBh
		defb	'DI$',		0F3h
		defb	'EI$',		0FBh
		defb	0
; Instructions with known opcode (no second stage processing)
; but with 2 bytes opcode (ED prefix)
imm_edprefx:	defb	'NEG$',		044h	; ED44h
		defb	'RETN$',	045h	; ...
		defb	'RETI$',	04Dh
		defb	'IM 0$',	046h
		defb	'LD I,A$',	047h
		defb	'LD R,A$',	04Fh
		defb	'IM 1$',	056h
		defb	'LD A,I$',	057h
		defb	'IM 2$',	05Eh
		defb	'LD A,R$',	05Fh
		defb	'RRD$',		067h
		defb	'RLD$',		06Fh
		defb	'IN (C)$',	070h
		defb	'IN F,(C)$',	070h
		defb	'OUT (C),0$',	071h
		defb	'LDI$',		0A0h
		defb	'CPI$',		0A1h
		defb	'INI$',		0A2h
		defb	'OUTI$',	0A3h
		defb	'LDD$',		0A8h
		defb	'CPD$',		0A9h
		defb	'IND$',		0AAh
		defb	'OUTD$',	0ABh
		defb	'LDIR$',	0B0h
		defb	'CPIR$',	0B1h
		defb	'INIR$',	0B2h
		defb	'OTIR$',	0B3h
		defb	'LDDR$',	0B8h
		defb	'CPDR$',	0B9h
		defb	'INDR$',	0BAh
		defb	'OTDR$',	0BBh
		defb	0
imm_ddprefx:
		defb	'INC IX$',	023h	; DD23h
		defb	'INC IXH$',	024h
		defb	'DEC IXH$',	025h
		defb	'INC IXL$',	02Ch
		defb	'DEC IXL$',	02Dh
		defb	'DEC IX$',	035h
		defb	'ADD A,IXH$',	084h
		defb	'ADD A,IXL$',	085h
		defb	'ADC A,IXH$',	08Ch
		defb	'ADC A,IXL$',	08Dh
		defb	'SUB A,IXH$',	094h
		defb	'SUB A,IXL$',	095h
		defb	'SBC A,IXH$',	09Ch
		defb	'SBC A,IXL$',	09Dh
		defb	'AND IXH$',	0A4h
		defb	'AND IXL$',	0A5h
		defb	'XOR IXH$',	0ACh
		defb	'XOR IXL$',	0ADh
		defb	'OR IXH$',	0B4h
		defb	'OR IXL$',	0B5h
		defb	'CP IXH$',	0BCh
		defb	'CP IXL$',	0BDh
		defb	'POP IX$',	0E1h
		defb	'EX (SP),IX$',	0E3h
		defb	'PUSH IX$',	0E5h
		defb	'JP (IX)$',	0E9h
		defb	'LD SP,IX$',	0F9h
		defb	0
imm_fdprefx:
		defb	'INC IY$',	023h	; FD23h
		defb	'INC IYH$',	024h
		defb	'DEC IYH$',	025h
		defb	'INC IYL$',	02Ch
		defb	'DEC IYL$',	02Dh
		defb	'DEC IY$',	035h
		defb	'ADD A,IYH$',	084h
		defb	'ADD A,IYL$',	085h
		defb	'ADC A,IYH$',	08Ch
		defb	'ADC A,IYL$',	08Dh
		defb	'SUB A,IYH$',	094h
		defb	'SUB A,IYL$',	095h
		defb	'SBC A,IYH$',	09Ch
		defb	'SBC A,IYL$',	09Dh
		defb	'AND IYH$',	0A4h
		defb	'AND IYL$',	0A5h
		defb	'XOR IYH$',	0ACh
		defb	'XOR IYL$',	0ADh
		defb	'OR IXH$',	0B4h
		defb	'OR IXL$',	0B5h
		defb	'CP IXH$',	0BCh
		defb	'CP IXL$',	0BDh
		defb	'POP IY$',	0E1h
		defb	'EX (SP),IY$',	0E3h
		defb	'PUSH IY$',	0E5h
		defb	'JP (IY)$',	0E9h
		defb	'LD SP,IY$',	0F9h
		defb	0
; Instructions to be parsed in two stages
twostages:	defb	'DJNZ$'			; <instruction id>, <jump address>
 		defw	parse_djnz
 		defb	'JR$'
 		defw	parse_jr
 		defb	'LD$'
 		defw	parse_ld
 		defb	'ADD$'
 		defw	parse_add
 		defb	'ADC$'
 		defw	parse_adc
 		defb	'SUB$'
 		defw	parse_sub
 		defb	'SBC$'
 		defw	parse_sbc
 		defb	'AND$'
 		defw	parse_and
 		defb	'XOR$'
 		defw	parse_xor
 		defb	'OR$'
 		defw	parse_or
 		defb	'CP$'
 		defw	parse_cp
 		defb	'DB$'
 		defw	parse_db
 		defb	'DW$'
 		defw	parse_dw
 		defb	'DS$'
 		defw	parse_ds
 		defb	'INC$'
 		defw	parse_inc
 		defb	'DEC$'
 		defw	parse_dec
 		defb	'RET$'
 		defw	parse_retcc
 		defb	'POP$'
 		defw	parse_pop
 		defb	'PUSH$'
 		defw	parse_push
 		defb	'JP$'
 		defw	parse_jp
 		defb	'CALL$'
 		defw	parse_call
 		defb	'RST$'
 		defw	parse_rst
 		defb	'RLC$'
 		defw	parse_rlc
 		defb	'RRC$'
 		defw	parse_rrc
 		defb	'RL$'
 		defw	parse_rl
 		defb	'RR$'
 		defw	parse_rr
 		defb	'SLA$'
 		defw	parse_sla
 		defb	'SRA$'
 		defw	parse_sra
 		defb	'SLL$'
 		defw	parse_sll
 		defb	'SRL$'
 		defw	parse_srl
 		defb	'IN$'
 		defw	parse_in
 		defb	'OUT$'
 		defw	parse_out
 		defb	'BIT$'
 		defw	parse_bit
 		defb	'RES$'
 		defw	parse_res
 		defb	'SET$'
 		defw	parse_set
 		defb	'MULUB$'
 		defw	parse_mulub
 		defb	'MULUW$'
 		defw	parse_muluw
 		defb	0


write_errmes:	defb	' Writing error!',13,10,'$'
load_errmes1:	defb	' is not found',13,10,'$'
load_errmes2:	defb	' is too large',13,10,'$'
load_mes:	defm	' Bytes load$'
write_mes:	defm	' Bytes writte$'
mes_ed:		defb	'ed',13,10,'$'
mes_en:		defb	'en',13,10,'$'

help_mes:	defb	'AS.DEBUG Ver.0.60 AriakiraSoft, resurrect by Emily82',13,10
		defb	' help     ?',13,10
		defb	' cpumode  # [0 | 1 | 2]',13,10
		defb	' assemble A [addres]',13,10
		defb	' compare  C range addres',13,10
		defb	' dump     D [range]',13,10
		defb	' enter    E [addres [value...]]',13,10
		defb	' fill     F range [value]',13,10
		defb	' go       G addres',13,10
		defb	' hex      H [value [value...]]',13,10
		defb	' input    I [port [number]]',13,10
		defb	' key      K {no function | on | off | !}',13,10
		defb	' load     L addres [drive sector number]',13,10
		defb	' move     M range addres2',13,10
		defb	' name     N [[drive:]filename[.type]]',13,10
		defb	' output   O port value [value...]',13,10
		defb	' register R [register value]',13,10
		defb	' search   S range',13,10
		defb	' trace    T addres [number]',13,10
		defb	' unasm    U [addres]',13,10
		defb	' write    W {address (BC=size)| address drive sector number}',13,10
		defb	' quit     Q',13,10,'$'
chrwrk:

	dephase

last:	end
